var page = require('./page');

var formPage = Object.create(page, {
    /**
     * define elements
     */
    searchButton: { get: function () { return browser.element('//*[@id="main-header"]/div/section[2]/a[1]/i');}},
    inputField:   { get: function () { return browser.element('//*[@id="search-panel"]/div/form/label/input');}},
    open:         { value: function() { page.open.call(this, '/'); }},
    flash:    { get: function () { return browser.element('//*[@id="main"]/form/p'); } }
});

module.exports = formPage;