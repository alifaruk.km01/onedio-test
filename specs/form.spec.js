var expect = require('chai').expect;
var FormPage = require('../pageobjects/form.page');

describe('auth form', function () {
    
    it('Login Button Test C11',function(){
        this.timeout(60000);
        FormPage.open();
        FormPage.loginButton.click();
        expect(FormPage.loginAreaChk.getText()).to.contain('Onedio Hesabı ile Giriş Yap');
    });
    
    it('Standart LoginTest C13', function () {
        FormPage.username.setValue('denemetesttest@yopmail.com');
        FormPage.password.setValue('123456789');
        FormPage.submitButton.click();
        FormPage.avatarForVisible(FormPage.loginChk,30000);

    });
    
});
